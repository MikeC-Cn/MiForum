from django.urls import path,include
from . import views

urlpatterns = [
    path('login/',views.Login,name="login"),
    path('register/',views.Register,name="register"),
    path('logout/',views.Logout,name="logout"),
    path('<key>/',views.Userinfo,name="userinfo"),
]