from django.db import models

# Create your models here.
class User(models.Model):
    key = models.IntegerField(default=0)
    username = models.CharField(max_length=16)
    password = models.CharField(max_length=26)
    email = models.EmailField()
    is_admin = models.BooleanField(default=False)
    is_superadmin = models.BooleanField(default=False)
    is_forbidden = models.BooleanField(default=False)
    color = models.CharField(max_length=7,default="#000000") # 黑 #000000 蓝 #0E90D2 绿 #5EB95E 橙 #F37B1D 红 #DD514C
    point = models.IntegerField(default=0) 
    description = models.TextField(default="这个人很懒，什么都没写......")


