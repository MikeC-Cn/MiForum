#coding:utf-8
from django.shortcuts import render,redirect
from . import models
import re

# Create your views here.
def Login(request):
    if request.session.get('is_login') == True:
        return redirect('home')
    if request.method == 'POST':
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        message = "请填写全部字段"
        if username and password:
            try:
                user = models.User.objects.get(username=username)
                if password == user.password:
                    request.session['is_login'] = True
                    request.session['key'] = user.key
                    request.session['username'] = username
                    request.session['is_admin'] = user.is_admin
                    request.session['is_superadmin'] = user.is_superadmin
                    request.session['is_forbidden'] = user.is_forbidden
                    request.session['color'] = user.color
                    return redirect('home')
                else:
                    message = "密码错误"
            except:
                message = "用户不存在"
        context = {}
        context['message'] = message
        return render(request,'user/login.html',context)
    return render(request,'user/login.html')

def Register(request):
    if request.method == 'POST' :
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        repassword = request.POST.get('repassword', None)
        email = request.POST.get('email', None)
        message = "请填写全部字段"
        if username and password and repassword and email:
            if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) == None:
                message = "邮箱格式不正确"
                context = {}
                context['message'] = message
                return render(request,'user/register.html',context)
            try:
                user = models.User.objects.get(username=username)
                message = "用户名已被注册"
            except:
                try:
                    user = models.User.objects.get(email=email)
                    message = "邮箱已被注册"
                except:
                    if password == repassword:
                        user = models.User.objects.create()
                        user.key = models.User.objects.all().count()
                        user.username = username
                        user.password = password
                        user.email = email
                        user.save()
                        return redirect('login')
                    else:
                        message = "两次输入的密码不一致"
        context = {}
        context['message'] = message
        return render(request,'user/register.html',context)
    return render(request,'user/register.html')

def Logout(request):
    request.session.flush()
    return redirect('home')

def Userinfo(request,key):
    if(request.session.get('is_login')):
        if(models.User.objects.get(username = request.session.get('username')).is_forbidden):
            request.session['is_forbidden'] = True
        else:
            request.session['is_forbidden'] = False
    if request.method == 'POST':
        description = request.POST.get('text', None)
        user = models.User.objects.get(key=key)
        user.description = description
        user.save()
        context = {}
        context['key'] = key
        context['username'] = user.username
        context['is_admin'] = user.is_admin
        context['is_forbidden'] = user.is_forbidden
        context['color'] = user.color
        context['point'] = str(int(user.point))
        context['description'] = user.description
        return render(request,'user/userinfo.html',context)
    try:
        user = models.User.objects.get(key=key)
        context = {}
        context['key'] = key
        context['username'] = user.username
        context['is_admin'] = user.is_admin
        context['is_forbidden'] = user.is_forbidden
        context['color'] = user.color
        context['point'] = user.point
        context['description'] = user.description
        return render(request,'user/userinfo.html',context)
    except:
        return redirect('home')
