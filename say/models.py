from django.db import models
from user.models import User

# Create your models here.
class Say(models.Model):
    key = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING,null=True)
    created_time = models.DateTimeField(auto_now_add=True)
    content = models.TextField()

    class Meta:
        ordering = ('-created_time',)
