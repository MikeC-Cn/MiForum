from django.urls import path
from . import views

urlpatterns = [
    path('',views.Say_list,name="say_list"),
    path('delete/<key>',views.Say_delete,name="say_delete"),
]