#coding:utf-8
from django.shortcuts import render,redirect
from . import models
from user.models import User

# Create your views here.
def Say_list(request):
    if(request.session.get('is_login')):
        if(User.objects.get(username = request.session.get('username')).is_forbidden):
            request.session['is_forbidden'] = True
        else:
            request.session['is_forbidden'] = False
    if request.method == 'POST':
        say = models.Say.objects.create()
        user = User.objects.get(username = request.session.get('username'))
        say.user = user
        say.content = request.POST.get('text')
        say.key = models.Say.objects.all().count()
        say.save()
        user.point = user.point + 3
        user.save()

    context = {}
    context['says'] = models.Say.objects.all()[:10]
    return render(request,'say/say_list.html',context)

def Say_delete(request,key):
    say = models.Say.objects.get(key=key)
    user = User.objects.get(username=say.user.username)
    user.point = user.point - 1
    user.save()
    say.delete()
    return redirect('say_list')

