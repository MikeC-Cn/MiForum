from django.urls import path
from . import views

urlpatterns = [
    path('',views.Store_list,name="store_list"),
    path('changecolor/<color>/',views.Changecolor,name="changecolor"),
]