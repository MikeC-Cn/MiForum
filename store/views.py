from django.shortcuts import render,redirect
from user.models import User

# Create your views here.
# def Changecolor(request,color):
def Store_list(request):
    if(request.session.get('is_login')):
        if(User.objects.get(username = request.session.get('username')).is_forbidden):
            request.session['is_forbidden'] = True
        else:
            request.session['is_forbidden'] = False
    context = {}
    if request.session.get('store_message'):
        context['message'] = request.session.get('store_message')
        del request.session['store_message']
    return render(request,'store/store.html',context)

def Changecolor(request,color):
    if(request.session.get('is_login')):
        if(User.objects.get(username = request.session.get('username')).is_forbidden):
            request.session['is_forbidden'] = True
        else:
            request.session['is_forbidden'] = False
    user = User.objects.get(username=request.session['username']) 
    if(user.point < 100):
        request.session['store_message'] = "咕咕币不足"
        return redirect('store_list')
    if(user.color == '#' + color):
        request.session['store_message'] = "您正在使用该颜色"
        return redirect('store_list')
    if color == "0E90D2" or color == "5EB95E" or color == "F37B1D" or color == "DD514C":
        user.color = '#' + color
        request.session['color'] = user.color
        user.point = user.point - 100
        request.session['point'] = user.point
        user.save()
        return redirect('store_list')
    else:
        return redirect('home')
