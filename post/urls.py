from django.urls import path
from . import views

urlpatterns = [
    path('',views.Plate_list,name="plate_list"),
    path('post/<key>/',views.Post_detail,name="post_detail"),
    path('plate/<key>/',views.Plate_detail,name="plate_detail"),
]