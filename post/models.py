from django.db import models
from user.models import User

# Create your models here.
class Post(models.Model):
    key = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True)
    created_time = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=50)
    content = models.TextField()
    plate = models.CharField(max_length=50)

    class Meta:
        ordering = ('-created_time',)

class Plate(models.Model):
    key = models.IntegerField(default=0)
    name = models.CharField(max_length=50,unique=True)

    def __str__(self):
        return self.name

class Comment(models.Model):
    key = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True)
    created_time = models.DateTimeField(auto_now_add=True)
    content = models.TextField()

    def __str__(self):
        return str(self.key)