#coding:utf-8
from django.shortcuts import render

def Home(request):
    context ={}
    context['color'] = request.session.get('color')
    return render(request,'home.html')